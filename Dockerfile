FROM ubuntu:22.04 as base

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y python3 python3-pip python-is-python3 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY pip.conf /etc/pip.conf
